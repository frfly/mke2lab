#include "FEM.h"
#include "LOSSolver.h"

using namespace std;

const int max_iter = 100000;
const double epsilon = 1e-16;

void print(vector<double> &x, ofstream& out);

int main(void)
{
	ofstream out("output.txt");
	ifstream inX("inputX.txt");
	ifstream inY("inputY.txt");
	ifstream borders("borders.txt");


	FEM F;
	LOSSolver S;
	vector<double> L, U, D;
	
	F.init(inX, inY, borders);
	F.assemblingGlobalMatrix();

	S.setStart(epsilon, max_iter);
	S.setMatrixA(F.di, F.al, F.au, F.ja, F.ia, F.N, F.rp);

	S.LUdec(L, U, D);
	S.LOS_LU(L, U, D);
	out << "Iter:" << S.iter << endl;
	print(S.x0.V, out);
}

void print(vector<double> &x, ofstream& out)
{
	out << scientific;
	out << setprecision(14);
	int j = 0;
	int k = 0;
	for (int i = 0; i < (int)x.size(); ++i)
		out << x[i] << endl;
}