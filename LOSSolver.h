#pragma once
#include "matrix.h"

using namespace std;

class LOSSolver
{
public:
	matrix A;
	vect p;
	vect z;
	vect r;
	vect x0;
	vect Ar;
	vect y;
	int maxiter;
	double eps;
	int iter;
	double normR;
	void make(int size);
	LOSSolver(void);
	~LOSSolver(void);
	void setStart(double epsilon, int maxIter);
	void setMatrixA(std::vector<double> di, std::vector<double> ggl,std::vector<double> ggu, std::vector<int> jg,std::vector<int> ig,int n,std::vector<double> rightPart);
	void LOS();
	void output(std::ofstream &X);
	void Direct(std::vector<double> &L, vect &y, vect &F);
	void Reverse(std::vector<double> &U, vect &x, vect &y);
	void Direct(std::vector<double> &L, std::vector<double> &D, vect &y, vect &F);
	void Reverse(std::vector<double> &U, std::vector<double> &D, vect &x, vect &y);
	void LUdec(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D);
	void LOS_LU(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D);
	void LU_SLAU(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D);
	void BSG_Stab(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D);
	void printA();
};