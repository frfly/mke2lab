#include "LOSSolver.h"

LOSSolver::LOSSolver(void)
{

}

LOSSolver::~LOSSolver(void)
{

}

void LOSSolver::make(int size)
{
	this->A.n = size;
	x0.make(A.n);
	r.make(A.n);
	z.make(A.n);
	p.make(A.n);
	Ar.make(A.n);
	y.make(A.n);
}

void LOSSolver::LOS()
{
	make(A.n);
	double scalPP = 0,scalPR = 0, scalRR = 0, scalPAr = 0;
	double a = 0, b = 0;
	bool exit = 1;
	A.multMV(x0,r);
	A.F - r;
	z = r;
	A.multMV(z,p);
	scalRR = r * r;
	normR = sqrt(scalRR) / A.normF; 
	for (iter = 1; iter < maxiter && exit != 0; iter++)
	{
		if( normR < eps || iter == maxiter - 1)
		{
			exit = 0;
			break;
		}
		scalPP = p * p;
		scalPR = p * r;
			a = scalPR/scalPP;
		for (int i = 0; i < A.n; i++)
		{
			x0.V[i] = x0.V[i] + z.V[i] * a;
			r.V[i] = r.V[i] - p.V[i] * a;
		}
		A.multMV(r, Ar);
		scalPAr = p * Ar;
		b = -(scalPAr/scalPP);
		for (int i = 0; i < A.n; i++)
		{
			z.V[i] = r.V[i] + z.V[i] * b;
			p.V[i] = Ar.V[i] + p.V[i] * b;
		}	
		//scalRR = scalRR - pow(a,2.0) * scalPP;
		scalRR = r * r;
		normR = sqrt(scalRR)/A.normF;
	}
}

void LOSSolver::LUdec(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D)
{
	L = A.ggl;
	U = A.ggu;
	D = A.di;
	double l, u, d;
	for(int k = 0; k < A.n; k++)
	{
		d = 0;
		int i0 = A.ig[k], i1 = A.ig[k + 1];
		int i = i0;
		for(; i0 < i1; i0++)
		{
			l = 0;
			u = 0;
			int j0 = i, j1 = i0;
			for(; j0 < j1; j0++)
			{
				int t0 = A.ig[A.jg[i0]], t1 = A.ig[A.jg[i0]+1];
				for (;   t0 < t1; t0++)
				{
					if (A.jg[j0] == A.jg[t0])
					{
						l += L[j0] * U[t0];
						u += L[t0] * U[j0];
					}
				}
			}
			L[i0] -= l;
			U[i0] -= u;
			U[i0] /= D[A.jg[i0]];
			d += L[i0] * U[i0];
		}
		D[k] -= d;
	}
}

void LOSSolver::Direct(std::vector<double> &L,std::vector<double> &D, vect &y, vect &F)
{
	y = F;
	for (int i = 0; i < A.n; i++)
	{
		double sum = 0;
		int k0 = A.ig[i], k1 = A.ig[i + 1];
		int j;
		for (; k0 < k1; k0++)
		{
			j = A.jg[k0];
			sum += y.V[j] * L[k0];
		}
		double buf = y.V[i] - sum;
		y.V[i] = buf / D[i];
	}
}

void LOSSolver::Reverse(std::vector<double> &U, vect &x, vect &y) 
{
	x = y;
	for(int i = A.n - 1; i >= 0; i--)
	{
		int k0 = A.ig[i], k1 = A.ig[i + 1];
		int j;
		for (; k0 < k1; k0++)
		{
			j = A.jg[k0];
			x.V[j] -= x.V[i]*U[k0];
		}
	}
}

void LOSSolver::Direct(std::vector<double> &L, vect &y, vect &F)
{
	y = F;
	for (int i = 0; i < A.n; i++)
	{
		double sum = 0;
		int k0 = A.ig[i], k1 = A.ig[i + 1];
		int j;
		for (; k0 < k1; k0++)
		{
			j = A.jg[k0];
			sum += y.V[j] * L[k0];
		}
		y.V[i] -= sum;
	}
}

void LOSSolver::Reverse(std::vector<double> &U, std::vector<double> &D, vect &x, vect &y) 
{
	x = y;
	for(int i = A.n - 1; i >= 0; i--)
	{
		int k0 = A.ig[i], k1 = A.ig[i + 1];
		int j;
		x.V[i] /= D[i];
		for (; k0 < k1; k0++)
		{
			j = A.jg[k0];
			x.V[j] -= x.V[i]*U[k0];
		}
	}
}

void LOSSolver::output(std::ofstream &X)
{
	X << iter << std::endl;
	X.precision(17);
	X << normR << std::endl;
	for (int i = 0; i < A.n; i++)
	{
		X << x0.V[i] << std::endl;
	}
}

void LOSSolver::LU_SLAU(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D)
{
	y = A.F;
	Direct(L, D, y, y);
	Reverse(U, x0, y);
}

void LOSSolver::setMatrixA(std::vector<double> di, std::vector<double> ggl,std::vector<double> ggu, std::vector<int> jg,std::vector<int> ig,int n,std::vector<double> rightPart)
{
	A.di = di;
	A.ig.reserve(ig.size());
	for(int i = 0; i < (int)ig.size(); ++i) A.ig.push_back(ig[i]);
	A.jg.reserve(jg.size());
	for(int i = 0; i < (int)jg.size(); ++i) A.jg.push_back(jg[i]);
	A.ggl = ggl;
	A.ggu = ggu;
	vect buf;
	buf.V = rightPart;
	A.F = buf;
	A.n = n;

	// �������� �������
	ofstream al("al.txt");
	ofstream au("au.txt");
	ofstream dii("di.txt");
	ofstream ia("ia.txt");
	ofstream ja("ja.txt");

	al << ggl.size() << endl;
	for(int i = 0; i < ggl.size(); ++i)
		al << ggl[i] << endl;

	au << ggu.size() << endl;
	for(int i = 0; i < ggu.size(); ++i)
		au << ggu[i] << endl;

	dii << di.size() << endl;
	for(int i = 0; i < di.size(); ++i)
		dii << di[i] << endl;

	ia << ig.size() << endl;
	for(int i = 0; i < ig.size(); ++i)
		ia << ig[i] << endl;

	ja << jg.size() << endl;
	for(int i = 0; i < jg.size(); ++i)
		ja << jg[i] << endl;
}

void LOSSolver::setStart(double epsilon, int maxIter)
{
	eps = epsilon;
	maxiter = maxIter;
}

void LOSSolver::BSG_Stab(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D)
{
	make(A.n);

	bool exit;

	double alfa;
	double gamma;
	double betta;
	double normB;
	double scalRR0; // r(k-1) * r(0)
	double scalPAr; // LAUp 

	vect rPrev;
	vect q;
	vect r0;
	vect LAUz; // LAUz
	vect LAUp; // LAUp
	
	exit = true;
	A.multMV(x0,y); // y = A * x
	A.F - y;		// y = F - y
	normB = y.norm();
	Direct(L,D,r0,y);// r = L-1 * y
	//Reverse(U,z,r); // z = U-1 * r
	z = r0;
	p = z;
	q = r0;
	r = r0;
	normR = r.norm() / normB;
	for (iter = 1; iter < maxiter && exit != 0; iter++)
	{
		if( normR < eps || iter == maxiter - 1)
		{
			exit = false;
			break;
		}
		scalRR0 = r * r0;
		rPrev = r; //r(k-1) ����������
		Reverse(U, y, z);
		A.multMV(y, Ar);
		Direct(L, D, Ar, Ar);
		LAUz =  Ar;
		alfa = scalRR0 / (r0 * LAUz);
		for (int i = 0; i < A.n; i++)
		{
			p.V[i] = r.V[i] - alfa * LAUz.V[i];
		}

		Reverse(U, y, p);
		A.multMV(y, Ar);
		Direct(L, D, Ar, Ar);
		LAUp =  Ar;
		gamma = (p * LAUp) / (LAUp * LAUp);

		for (int i = 0; i < A.n; i++)
		{
			q.V[i] = q.V[i] + alfa * z.V[i] + gamma * p.V[i];
			r.V[i] = p.V[i] - gamma * LAUp.V[i];
		}

		betta = (alfa * (r * r0)) / (gamma * scalRR0); // ��� ����� �����
		for (int i = 0; i < A.n; i++)
		{
			z.V[i] = r.V[i] + betta * rPrev.V[i] - betta * gamma * LAUz.V[i];
		}
		normR = r.norm() / normB;
	}
}

void LOSSolver::LOS_LU(std::vector<double> &L, std::vector<double> &U, std::vector<double> &D)
{
	make(A.n);
	double scalPP = 0,scalPR = 0, scalRR = 0, scalPAr = 0;
	double a = 0, b = 0;
	bool exit = 1;
	A.multMV(x0,y); // y = A * x
	A.F - y;		// y = F - y
	Direct(L,D,r,y);// r = L-1 * y
	Reverse(U,z,r); // z = U-1 * r
	A.multMV(z,y);  // y = A * z
	Direct(L,D,p,y);// p = L-1 * y 
	scalRR = r * r;
	normR = sqrt(scalRR) / A.F.norm();
	for (iter = 1; iter < maxiter && exit != 0; iter++)
	{
		if( normR < eps || iter == maxiter - 1)
		{
			exit = 0;
			break;
		}
		scalPP = p * p;
		scalPR = p * r;
			a = scalPR/scalPP;
		for (int i = 0; i < A.n; i++)
		{
			x0.V[i] = x0.V[i] + z.V[i] * a;
			r.V[i] = r.V[i] - p.V[i] * a;
		}
		Reverse(U, y, r);
		A.multMV(y, Ar);
		Direct(L, D, Ar, Ar);
		scalPAr = p * Ar;
		b = -(scalPAr/scalPP);
		for (int i = 0; i < A.n; i++)
		{
			z.V[i] = y.V[i] + z.V[i] * b;
			p.V[i] = Ar.V[i] + p.V[i] * b;
		}	
		//scalRR = scalRR - pow(a,2.0) * scalPP;
		scalRR = r * r;
		normR = sqrt(scalRR)/A.F.norm();
	}
}