#pragma once

#include <vector>
#include <fstream>
#include <iomanip>

struct fElem
{
	int x;	// ������� ��������� 
	int y;	// � ��������������� ��������
};

struct listone
{
	int node;
	listone *next;
};

struct border
{
	int type;
	int direction;

	double leftXCoord;
	double leftYCoord;
	double rightXCoord;
	double rightYCoord;
};

class FEM
{
public:
	int N;
	int Nx;
	int Ny;
	int Nf;
	int gNumsFem[4];

	double hx;
	double hy;
	double M[4][4];
	double G[4][4];

	std::vector<int> ia;
	std::vector<int> ja;

	std::vector<double> au;
	std::vector<double> al;
	std::vector<double> di;
	std::vector<double> rp;
	std::vector<double> gridX;
	std::vector<double> gridY;
	std::vector<double> localRp;
	std::vector<double> intervalsX;
	std::vector<double> intervalsY;

	std::vector<fElem> fMas;
	std::vector<border> bords;

	FEM();
	~FEM();

	int getGlobNum(int ix, int jy);
	int lineSearch(std::vector<double> &A, double &val);

	double f(double x, double y);
	double u(double x, double y);

	void setBorders();
	void firstBord(int i);
	void secondBord(int i);
	void generatePortrait();
	void setGlobNums(int &i);
	void calcLocalVector(int i);
	void assemblingGlobalMatrix();
	void calcFirstBords(int x, int y);
	void addToGlobal(int &i, int &j, double val);
	void insert(listone &elem, int global, int &Nj);
	void gridCheck(std::vector<double> &grid, double &end, double &storage);
	void init(std::ifstream& inputX, std::ifstream& inputY, std::ifstream& borders);
	void assemblingMassAndRigityMatrixes(double hx, double hy, double lambda, double gamma);
	void generateGrid(double begin, double end, double coefficient, double step, std::vector<double>& grid);
};