#include"FEM.h"

using namespace std;

FEM::FEM()
{

}
FEM::~FEM() 
{

}
void FEM::init(std::ifstream& inputX, std::ifstream& inputY, std::ifstream& borders)
{
	double step;
	int limitX;
	int limitY;
	int numOfIntervals;

	double end;
	double begin;
	double coefficient;

	fElem FE;
	border b;

	if (inputX.is_open())
	{
		inputX >> numOfIntervals;
		intervalsX.reserve(numOfIntervals + 1);
		for (int i = 0; i < numOfIntervals; ++i)
		{
			inputX >> begin >> end >> coefficient >> step;
			generateGrid(begin, end, coefficient, step, gridX);
			if (i == 0) intervalsX.push_back(begin);
			intervalsX.push_back(end);
		}
	}
	else exit(1);

	if (inputY.is_open())
	{
		inputY >> numOfIntervals;
		intervalsY.reserve(numOfIntervals + 1);
		for (int i = 0; i < numOfIntervals; ++i)
		{
			inputY >> begin >> end >> coefficient >> step;
			generateGrid(begin, end, coefficient, step, gridY);
			if (i == 0) intervalsY.push_back(begin);
			intervalsY.push_back(end);
		}
	}
	else exit(1);

	if (borders.is_open())
	{
		borders >> numOfIntervals;
		bords.reserve(numOfIntervals);
		for (int i = 0; i < numOfIntervals; ++i)
		{
			borders >> b.type >> b.leftXCoord >> b.leftYCoord >>
				b.rightXCoord >> b.rightYCoord >> b.direction;
			bords.push_back(b);
		}
	}

	Nx = gridX.size();
	Ny = gridY.size();
	N = Nx * Ny;
	Nf = (Nx - 1) * (Ny - 1);
	fMas.reserve(Nf);

	limitX = Nx - 1;
	limitY = Ny - 1;
	for (int i = 0; i < limitY; i++)
		for (int j = 0; j < limitX; j++)
			{
				FE.x = j;
				FE.y = i;
				fMas.push_back(FE);
			}
}
void FEM::generateGrid(double begin, double end, double coefficient, double step, std::vector<double>& grid)
{
	int size;
	double storage;

	size = (int)((end - begin) / step + 1);
	grid.reserve(size);

	grid.push_back(begin);
	storage = begin + step;
	grid.push_back(storage);

	if (coefficient == 1)
		for (int i = 2; i < size; ++i)
		{
			(i == size - 1) ? storage = end: storage = step * i;
			grid.push_back(storage);
		}
	else
	{
		bool flag = true;
		while (flag)
		{
			step *= coefficient;
			storage += step;
			(storage >= end) ? gridCheck(grid, end, storage) : grid.push_back(storage);
			if (grid.back() == end) flag = false;
		}
	}
}
void FEM::gridCheck(vector<double> &grid, double &end, double &storage)
{
	double actualStep = storage - grid.back();
	double prevStep = grid.back() - grid[grid.size() - 2];
	if (storage == end) grid.push_back(end);
	else if (storage > end)
		if (end - grid.back() > prevStep) grid.push_back(end);
		else
		{
			grid.pop_back();
			grid.push_back(end);
		}
}
void FEM::generatePortrait()
{
	int counter;
	listone *run;
	listone *list;

	int Nj = 0;
	counter = 0;
	list = new listone[N];

	for (int i = 0; i < N; i++)
	{
		list[i].node = -1;
		list[i].next = NULL;
	}

	for (int i = 0; i < Nf; i++)
	{
		setGlobNums(i);
		for (int j = 0; j < 4; j++)
			for (int k = j + 1; k < 4; k++)
				if (gNumsFem[j] < gNumsFem[k]) insert(list[gNumsFem[k]], gNumsFem[j], Nj);
	}
		
	ia.assign(N + 1, 0);
	ja.assign(Nj, 0);

	for (int i = 0; i < N; i++)
	{
		run = list[i].next;
		while (run != NULL)
		{
			ja[counter] = run->node;
			run = run->next;
			counter++;
		}
		ia[i + 1] = counter;
	}
}
void FEM::insert(listone &elem, int global, int &Nj)
{
	bool exit;
	listone *prev;
	listone *buf;	//��� ���������� �������������� ����������
	listone *run;	//�������
	listone *addend;//����������� ������� � ������

	exit = false;
	addend = new listone;
	addend->node = global;
	addend->next = NULL;

	if (elem.next == NULL)
	{
		elem.next = addend;
		exit = true;
		Nj++;
	}

	run = elem.next;
	prev = run;
	while (!exit)
	{
		if (run->node == global) exit = true;
		if (run->node < global && run->next == NULL)
		{
			run->next = addend;
			exit = true;
			Nj++;
		}
		if (run->node > global)
		{
			buf = run;
			if (elem.next == run) elem.next = addend;
			else prev->next = addend;
			addend->next = buf;
			exit = true;
			Nj++;
		}
		prev = run;
		run = run->next;
	}
}
void FEM::setGlobNums(int &i)
{
	gNumsFem[0] = getGlobNum(fMas[i].x, fMas[i].y);
	gNumsFem[1] = getGlobNum(fMas[i].x + 1, fMas[i].y);
	gNumsFem[2] = getGlobNum(fMas[i].x, fMas[i].y + 1);
	gNumsFem[3] = getGlobNum(fMas[i].x + 1, fMas[i].y + 1);
}
int FEM::getGlobNum(int ix, int jy)
{
	return jy * Nx + ix;
}
void FEM::assemblingGlobalMatrix()
{
	int ii, jj;

	generatePortrait();
	
	rp.reserve(N);
	localRp.reserve(4);
	di.assign(N, 0);
	al.assign(ja.size(), 0);
	au.assign(ja.size(), 0);
	rp.assign(N, 0);
	localRp.assign(4, 0);

	for (int i = 0; i < Nf; i++)
	{
		hx = gridX[fMas[i].x + 1] - gridX[fMas[i].x];
		hy = gridY[fMas[i].y + 1] - gridY[fMas[i].y];

		setGlobNums(i);
		assemblingMassAndRigityMatrixes(hx, hy, 1, 0);
		calcLocalVector(i);

		for (int j = 0; j < 4; j++)
		{
			for (int k = 0; k < 4; k++)
			{
				ii = gNumsFem[j];
				jj = gNumsFem[k];
				addToGlobal(ii, jj, G[j][k] + M[j][k]);
			}
			rp[gNumsFem[j]] += localRp[j];
		}
	}
	setBorders();
}
void FEM::assemblingMassAndRigityMatrixes(double hx, double hy, double lambda, double gamma)
{
	G[0][0] = (lambda * hy) / (6.0 * hx) * (2.0 + 2.0);
	G[0][1] = (lambda * hy) / (6.0 * hx) * (-2.0 + 1.0);
	G[0][2] = (lambda * hy) / (6.0 * hx) * (1.0 - 2.0);
	G[0][3] = (lambda * hy) / (6.0 * hx) * (-1.0 - 1.0);

	G[1][0] = G[0][1]; G[1][1] = G[0][0]; G[1][2] = G[0][3]; G[1][3] = G[0][2];
	G[2][0] = G[0][2]; G[2][1] = G[0][3]; G[2][2] = G[0][0]; G[2][3] = G[0][1];
	G[3][0] = G[0][3]; G[3][1] = G[0][2]; G[3][2] = G[0][1]; G[3][3] = G[0][0];

	M[0][0] = gamma * hx * hy / 36 * 4;
	M[0][1] = gamma * hx * hy / 36 * 2;
	M[0][2] = gamma * hx * hy / 36 * 2;
	M[0][3] = gamma * hx * hy / 36 * 1;

	M[1][0] = M[0][1]; M[1][1] = M[0][0]; M[1][2] = M[0][3]; M[1][3] = M[0][1];
	M[2][0] = M[0][1]; M[2][1] = M[0][3]; M[2][2] = M[0][0]; M[2][3] = M[0][1];
	M[3][0] = M[0][3]; M[3][1] = M[0][1]; M[3][2] = M[0][1]; M[3][3] = M[0][0];
}
void FEM::calcLocalVector(int i)
{
	localRp[0] = (4 * f(gridX[fMas[i].x],gridY[fMas[i].y]) + 2 * f(gridX[fMas[i].x + 1], gridY[fMas[i].y]) +
		          2 * f(gridX[fMas[i].x], gridY[fMas[i].y + 1]) + f(gridX[fMas[i].x + 1], gridY[fMas[i].y + 1])) *
		         (hx * hy / 36);
	localRp[1] = (2 * f(gridX[fMas[i].x], gridY[fMas[i].y]) + 4 * f(gridX[fMas[i].x + 1], gridY[fMas[i].y]) +
				  f(gridX[fMas[i].x], gridY[fMas[i].y + 1]) + 2 * f(gridX[fMas[i].x + 1], gridY[fMas[i].y + 1])) *
				 (hx * hy / 36);
	localRp[2] = (2 * f(gridX[fMas[i].x], gridY[fMas[i].y]) + f(gridX[fMas[i].x + 1], gridY[fMas[i].y]) +
				  4 * f(gridX[fMas[i].x], gridY[fMas[i].y + 1]) + 2 * f(gridX[fMas[i].x + 1], gridY[fMas[i].y + 1])) *
				 (hx * hy / 36);
	localRp[3] = (f(gridX[fMas[i].x], gridY[fMas[i].y]) + 2 * f(gridX[fMas[i].x + 1], gridY[fMas[i].y]) +
				  2 * f(gridX[fMas[i].x], gridY[fMas[i].y + 1]) + 4 * f(gridX[fMas[i].x + 1], gridY[fMas[i].y + 1])) *
				 (hx * hy / 36);
}
double FEM::f(double x, double y)
{
	return -4;
}
double FEM::u(double x, double y)
{
	return x*x + y*y;
}
void FEM::addToGlobal(int &i, int &j, double val)
{
	int ind;
	int limit;
	if (i == j) di[i] += val;
	else
	{
		if (i < j)
		{
			for (ind = ia[j], limit = ia[j + 1]; i < limit; ind++)
				if (ja[ind] == i) break;
			au[ind] += val;
		}
		else
		{
			for (ind = ia[i], limit = ia[i + 1]; i < limit; ind++)
				if (ja[ind] == j) break;
			al[ind] += val;
		}
	}
}
int FEM::lineSearch(vector<double> &A, double &val)
{
	int N = A.size();
	for (int i = 0; i < N; ++i)
		if (A[i] == val) return i;
	return -999999;
}
void FEM::setBorders()
{
	int n = bords.size();
	for (int i = 0; i < n; ++i)
		(bords[i].type == 1) ? firstBord(i) : secondBord(i);
}
void FEM::firstBord(int i)
{
	int iY1;
	int iY2;
	int iX1;
	int iX2;
	switch (bords[i].direction)
	{
	case 0:
	{
		iY1 = lineSearch(gridY, bords[i].leftYCoord);
		iX1 = lineSearch(gridX, bords[i].leftXCoord);
		iX2 = lineSearch(gridX, bords[i].rightXCoord);

		for (int i = iX1; i <= iX2; ++i)
			calcFirstBords(i, iY1);
		break;
	}
	case 1:
	{
		iY1 = lineSearch(gridY, bords[i].leftYCoord);
		iY2 = lineSearch(gridY, bords[i].rightYCoord);
		iX1 = lineSearch(gridX, bords[i].leftXCoord);
		for (int i = iY1; i <= iY2; ++i)
			calcFirstBords(iX1, i);
		break;
	}
	}
}
void FEM::calcFirstBords(int x, int y)
{
	int i;
	int k;
	int j;

	i = getGlobNum(x, y);
	di[i] = 1;
	rp[i] = u(gridX[x], gridY[y]);
	for (k = ia[i]; k < ia[i + 1]; k++) al[k] = 0;
	for (k = i + 1; k < N; k++)
	{
		for (j = ia[k]; j < ia[k + 1]; j++)
			if (ja[j] == i) au[j] = 0;
	}
}
void FEM::secondBord(int i)
{

}