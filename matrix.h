#pragma once

#include "vect.h"

class matrix
{
public:
	std::vector<double> di;
	std::vector<double> ggl;
	std::vector<double> ggu;
	vect F;
	std::vector<int> jg;
	std::vector<int> ig;
	int n;
	double normF;
	matrix(void);
	void make(std::ifstream &size);
	~matrix(void);
	void multMV(vect &X, vect &Y);
	void multMTV(vect &X, vect &Y);
	void divDi(vect &X);
	void Hilbert(std::ifstream &size);
};